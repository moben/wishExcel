package com.wish.demo;

/**
 * @author tantexian
 * @since 2016/7/3
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.SimpleFormatter;


public class MyFileChooser implements ActionListener {

    String DefaultOutPath = "d:\\";
    String DefaultOutFileName = "考勤统计结果.xlsx";
    String inputPath = null;
    String outputPath = DefaultOutPath + DefaultOutFileName;

    JFrame frame = new JFrame("考勤Excel统计分析");
    JTabbedPane tabPane = new JTabbedPane();// 选项卡布局
    Container container = new Container();// 布局
    JLabel labelIn = new JLabel("选择待处理Excel原始文件");
    JLabel labelOut = new JLabel("选择输出目录或excel文件");
    JTextField textIn = new JTextField();
    JTextField textOut = new JTextField();
    JButton buttonIn = new JButton("...");
    JButton buttonOut = new JButton("...");
    JButton buttonOpenWinFile = new JButton("打开文件");

    JFileChooser jfc = new JFileChooser();// 文件选择器

    JTextField textResult = new JTextField();

    JLabel labelResult = new JLabel("当前状态：");

    JButton executeButton = new JButton("生成考勤统计结果(默认文件名为:" + DefaultOutFileName + ")");


    MyFileChooser() {
        jfc.setCurrentDirectory(new File(DefaultOutPath));// 文件选择器的初始目录定为d盘
        String nowPath = DefaultOutPath + DefaultOutFileName;
        textOut.setText(nowPath);

        // 下面两行是取得屏幕的高度和宽度
        double lx = Toolkit.getDefaultToolkit().getScreenSize().getWidth();
        double ly = Toolkit.getDefaultToolkit().getScreenSize().getHeight();
        frame.setLocation(new Point((int) (lx / 3), (int) (ly / 3)));// 设定窗口出现位置
        frame.setSize((int) (lx / 3), (int) (ly / 3));// 设定窗口大小
        frame.setContentPane(tabPane);// 设置布局
        // 下面设定标签等的出现位置和高宽
        int width = (int) (lx / 6);
        int height = 25;
        labelIn.setBounds(10, 10, width, height);
        labelOut.setBounds(10, 45, width, height);
        textIn.setBounds(165, 10, width + 10, height);
        textOut.setBounds(165, 50, width + 10, height);
        buttonIn.setBounds(width + 180, 10, 50, height);
        buttonOut.setBounds(width + 180, 50, 50, height);
        labelResult.setBounds(10, 100, 400, height);
        buttonOpenWinFile.setBounds(width + 180, 100, 100, height);
        labelResult.setFont(new Font("标楷体", Font.BOLD, 16));

        textResult.setFont(new Font("标楷体", Font.BOLD, 16));
        textResult.setBackground(Color.green);

        labelResult.setBackground(Color.MAGENTA);
        executeButton.setBounds(80, 200, 400, height);

        textResult.setBounds(10, 140, 600, 40);

        buttonIn.addActionListener(this);// 添加事件处理
        buttonOut.addActionListener(this);
        buttonOpenWinFile.addActionListener(this);
        executeButton.addActionListener(this);

        container.add(labelIn);
        container.add(labelOut);
        container.add(textIn);
        container.add(textOut);
        container.add(textResult);
        container.add(buttonIn);
        container.add(buttonOut);
        container.add(executeButton);
        container.add(jfc);
        container.add(labelResult);
        container.add(buttonOpenWinFile);

        buttonOpenWinFile.setVisible(false);

        tabPane.add("输入输出文件选择", container);// 添加布局1
        frame.setVisible(true);// 窗口可见
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// 使能关闭窗口，结束程序
        textResult.setText("等待用户选择输入输出目录/文件ing");

    }


    public void actionPerformed(ActionEvent e) {// 事件处理
        if (e.getSource().equals(buttonIn)) {// 判断触发方法的按钮是哪个

            jfc.setFileSelectionMode(0);// 设定只能选择文件
            int state = jfc.showOpenDialog(null);// 此句是打开文件选择器界面的触发语句
            if (state == 1) {
                return;// 撤销则返回
            }
            else {
                File file = jfc.getSelectedFile();// file为选择到的目录
                inputPath = file.getAbsolutePath();
                String info = "当前选择Excel输入文件为：" + inputPath;
                textResult.setText(info);
                System.out.println(info);
                textIn.setText(inputPath);
            }
        }
        else if (e.getSource().equals(buttonOut)) {
            jfc.setFileSelectionMode(2);// 设定可以选择文件或者目录
            int state = jfc.showOpenDialog(null);// 此句是打开文件选择器界面的触发语句
            if (state == 1) {
                return;// 撤销则返回
            }
            else {
                File file = jfc.getSelectedFile();// file为选择到的文件
                if (file.isDirectory()) {
                    // String dateStr = (new SimpleDateFormat("yyyy-MM-dd
                    // hh:mm:ss")).format(new Date());
                    outputPath = file.getAbsolutePath() + "\\" + DefaultOutFileName;

                }
                else {
                    outputPath = file.getAbsolutePath();
                }

                String info = "当前选择Excel输出路径为：" + outputPath;
                textResult.setText(info);
                System.out.println(info);

                textOut.setText(outputPath);
            }
        }
        else if (e.getSource().equals(buttonOpenWinFile)) {
            openWinFile(outputPath);
        }
        else if (e.getSource().equals(executeButton)) {
            final String info = "当前:  [输入文件路径：" + inputPath + "]  |  [输出文件路径：" + outputPath + "]";
            System.out.println(info);
            textResult.setText(info);

            if (inputPath == null) {
                textResult.setText("当前状态： 当前Excel输入文件为空，请重新选择！！！");
                return;
            }

            // 如果文件不存在，则创建文件
            try {
                new File(outputPath).createNewFile();
            }
            catch (IOException e1) {
                textResult.setText("创建文件(" + outputPath + ")失败，请检查路径是否配置正确！！！PS:" + e.toString());
                e1.printStackTrace();
                return;
            }

            File inputFile = new File(inputPath);
            File outputFile = new File(outputPath);
            if (!inputFile.exists()) {
                textResult.setText("输入文件无法打开，请确认文件目录是否正确且文件是否存在！！！");
                return;
            }

            if (!outputFile.exists()) {
                textResult.setText("输出文件无法打开，请确认文件目录是否正确且文件是否存在！！！");
                return;
            }

            try {
                String text = "当前状态：正在处理中，请耐心等待（大约需要几分钟）...";
                // 没有生效？？？
                labelResult.setText(text);

                System.out.println(text);

                Startup.createResultExcel(inputPath, outputPath);

            }
            catch (IOException e1) {
                textResult.setText("当前状态： [异常]" + e1.toString());
                e1.printStackTrace();
                return;
            }
            catch (ParseException e1) {
                textResult.setText("当前状态： [异常]" + e1.toString());
                e1.printStackTrace();
                return;
            }

            System.out.println("生成统计结果！！！");
            labelResult.setText("当前状态：执行成功，请注意查看!!!");
            textResult.setText("考勤统计结果Excel生成成功，请注意查看, [路径：" + outputPath + "]");
            buttonOpenWinFile.setVisible(true);
        }
    }


    // 打开windows文件夹
    private static void openWinFile(String path) {
        // noinspection Since15
        Desktop desktop = Desktop.getDesktop();

        // noinspection Since15
        try {
            // noinspection Since15
            desktop.open(new File(path));
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
