package com.wish.demo;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;

/**
 * Idea 生成可执行Jar：
 * 1、选中Java项目工程名称，在菜单中选择 File->project structure... (快捷键Ctrl+Alt+Shift+S)。
 * 2、在弹出的窗口中左侧选中"Artifacts"，点击"+"选择jar，然后选择"from modules with dependencies"。
 * 3、在配置窗口中配置"Main Class"。
 * 4、务必选择“Main Class”后，选择"copy to the output  and link via manifest"
 * 5、务必将"Directory for META-INF/MAINFEST.MF"其缺省值是：XXX\src\main\java 修改为：XXX\src\main\resources
 * 6、点击确定。跳转到打包的配置项页面，将output Directory修改为你想要输出jar的路径
 * 6、回到Idea主界面，Build->Build Airtifacts
 * 7、打包好的jar在，6中设置好的output Directory
 * 更多详细请参考：http://my.oschina.net/tantexian/blog/705078
 *
 * @author tantexian
 * @since 2016/7/3
 */
public class Startup {
    public static void main(String[] args) {
        new MyFileChooser();
    }


    public static void createResultExcel(String inputPath, String outputPath) throws IOException, ParseException {


        DoExcel.main0(inputPath, outputPath);
    }
}
