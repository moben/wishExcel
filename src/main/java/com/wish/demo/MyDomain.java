package com.wish.demo;

/**
 * @author tantexian
 * @since 2016/7/1
 */
public class MyDomain {
    private Integer id;
    private Integer xid;
    private String departName;
    private Integer kqId;
    private String name;

    public String getDepartName() {
        return departName;
    }

    public void setDepartName(String departName) {
        this.departName = departName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getKqId() {
        return kqId;
    }

    public void setKqId(Integer kqId) {
        this.kqId = kqId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getXid() {
        return xid;
    }

    public void setXid(Integer xid) {
        this.xid = xid;
    }
}
