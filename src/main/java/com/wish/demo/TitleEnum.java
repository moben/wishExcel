package com.wish.demo;

/**
 * @author tantexian
 * @since 2016/7/1
 */
public enum TitleEnum {
    XH("序号", 1),
    KQJBH("考勤机编号",2),
    KQID("部门名称", 3),
    NAME("序号", 4),
    TIME("序号", 5),
    STATUS("序号", 6),
    FS("序号", 7),
    SBNAME("序号", 8);

    private String name;
    private int index;
    TitleEnum(String name, int index){
        this.name = name;
        this.index = index;
    }

    public int getIndex(){
        return index;
    }
}
